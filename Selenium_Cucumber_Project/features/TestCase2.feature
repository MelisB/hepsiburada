#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Adding an item to the bucket with Logged-in user

  @tag1
  Scenario: User has an account, makes search and adds two items to bucket from different sellers
    Given User opens chrome HepsiBurada.com page
    And User logs in
    And Log in validation
    When User searchs something
    When User selects random book
    When User adds book to the bucket from two different sellers
    Then Validate the bucket
