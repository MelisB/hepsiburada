package stepDefinition2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import java.util.List;
import java.util.Random;

public class TestCase2 {
	WebDriver driver;
	public WebDriverWait wait;
	
	
	@Given("^User opens chrome HepsiBurada.com page$")
	public void openPage() throws Throwable {
		System.out.println("*******************");
		System.out.println("launching chrome browser");
		String exePath = "C:\\Users\\melis\\Desktop\\Test Project\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 600);
		driver.manage().window().maximize();  
		driver.get("https://www.hepsiburada.com/");
		String link = "https://www.hepsiburada.com/";
		String url = driver.getCurrentUrl();
		Assert.assertEquals(url,link);
		
	}
	
	@And("^User logs in$")
	public void logIn() throws Throwable {
	wait.until(ExpectedConditions.elementToBeClickable(By.id("myAccount")));
	driver.findElement(By.id("myAccount")).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.id("login")));
	driver.findElement(By.id("login")).click();
	wait.until(ExpectedConditions.urlContains("giris"));
	//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='login-container']/section/div")));
	//wait.until(ExpectedConditions.elementToBeClickable(By.id("email")));
	Thread.sleep(2000);
	driver.findElement(By.xpath("//div[@id='login-container']/section/header/h1")).getText().contains("Giri�i");
	driver.findElement(By.id("email")).click();
	driver.findElement(By.id("email")).sendKeys("work.melisbayram@gmail.com");
	driver.findElement(By.id("password")).click();
	driver.findElement(By.id("password")).sendKeys("123456testtest");
	driver.findElement(By.xpath("//button[@type='submit']")).click();
	}
	
	@And("Log in validation")
	public void validateLogIn() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("a.logo-hepsiburada > span")));
		List<WebElement> lists = driver.findElements(By.className("usersProsess"));
		Assert.assertEquals(lists.get(0).getText(), "Melis Bayram");
	}
	
	@When("^User searchs something$")
	public void makeSearch() throws Throwable {
		driver.findElement(By.id("productSearch")).clear();
		driver.findElement(By.id("productSearch")).sendKeys("kitap");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("a.rfk_open_search_page")));
		driver.findElement(By.cssSelector("a.rfk_open_search_page")).click();
		wait.until(ExpectedConditions.urlContains("kitap"));
	}
	@When("^User selects random book$")
	public void selectRandomBook() throws Throwable {
		
		Random rnd = new Random();
		int ktp = rnd.nextInt(10);
		driver.findElement(By.xpath("//li["+(ktp+1)+"]/div/a/figure/img")).click();
	}
	@When("^User adds book to the bucket from two different sellers$")
	public void addBook() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(By.id("addToCart")));
		driver.findElement(By.xpath("//form/button")).click();;
		wait.until(ExpectedConditions.urlContains("sepetim"));
		wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Al��veri�e Devam Et")));
		driver.findElement(By.xpath("//form[@id='form-item-list']/ul/li/div/div/h4/a")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("addToCart")));
		driver.findElement(By.xpath("//div[4]/div[2]/div[2]/div/div/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//tr[3]/td[7]/form/button")).click();
		
		
	}
	@Then("^Validate the bucket$")
	public void bucketValidation() throws Throwable {
		wait.until(ExpectedConditions.urlContains("sepetim"));
		Assert.assertEquals(driver.findElement(By.xpath("//div[@id='short-summary']/div/p/span")).getText(), "2 �r�n");
		driver.quit();
	}
}
