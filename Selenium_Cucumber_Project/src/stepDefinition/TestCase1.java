package stepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;

public class TestCase1 {
	
	WebDriver driver;
	public WebDriverWait wait;
	
	
	@Given("^User opens HepsiBurada.com page.$")
	public void openPage() throws Throwable {
		System.out.println("*******************");
		System.out.println("launching chrome browser");
		String exePath = "C:\\Users\\melis\\Desktop\\Test Project\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 600);
		driver.manage().window().maximize();  
		driver.get("https://www.hepsiburada.com/");
		String link = "https://www.hepsiburada.com/";
		String url = driver.getCurrentUrl();
		Assert.assertEquals(url,link);
		}

	@And("^User clicks on Kitap, Muzik, Film, Hobi$")
	public void clickOnCategory() throws Throwable {
		Thread.sleep(2000);
		if(driver.findElement(By.linkText("K�TAP, M�Z�K F�LM, HOB�")).isDisplayed() == true) 
		{
			driver.findElement(By.xpath("//li[@id='kitap-muzik-film-hobi']/a")).click();
			Thread.sleep(1000);
			if(driver.findElement(By.xpath("//li[@id='kitap-muzik-film-hobi']/div")).isDisplayed() == true) 
			{
				wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.linkText("Uzaktan Kumandal� Ara�lar"))));
				driver.findElement(By.linkText("Uzaktan Kumandal� Ara�lar")).click();
			}
	    }
	}

	@When("^Select an item$")
	public void ClickandAddDrone() throws Throwable {
		Thread.sleep(2000);
		String link2 = "https://www.hepsiburada.com/uzaktan-kumandali-arabalar-c-14001244";
		String url2 = driver.getCurrentUrl();
		Assert.assertEquals(url2,link2);
		Thread.sleep(3000);
		driver.findElement(By.xpath("//li[2]/div/a/div/div[2]/span")).click();
		wait.until(ExpectedConditions.urlToBe("https://www.hepsiburada.com/rcx-xx6-drone-u-k-gece-goruslu-quadcopter-p-OYUNREELXX6BYZ"));
		
		/*
		Actions builder = new Actions(driver);
		builder.moveToElement((WebElement) By.xpath("//div[@id='0c38a0ce-398a-4deb-a66c-1df01baafb26']/div/div/ul/li[2]/div")).perform();
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector("button.add-to-basket.button.small"))));
		builder.moveToElement((WebElement) By.cssSelector("button.add-to-basket.button.small")).click().perform();
		 */
			}
	
	@Then("^Validation of the product$")
	public void bucketValidation() throws Throwable {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("addToCart")));
		String product = driver.findElement(By.id("product-name")).getText();
		driver.findElement(By.id("addToCart")).click();
		wait.until(ExpectedConditions.urlContains("sepetim"));
		driver.findElement(By.cssSelector("h4.product-name > a")).getText().contains(product);
		driver.quit();
	}


}
